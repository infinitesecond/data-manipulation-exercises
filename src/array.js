/**
 * A representation for an Autochess Piece
 * @typedef {Object} ChessPiece
 * @property {string} name - Name of the chess piece
 * @property {string} species - Species/race of the chess piece
 * @property {string} class - class of the chess piece
 * @property {number} cost - amount of gold needed to purchase chess piece
 */

/** 
 * Add a Naga/Hunter piece named Tidehunter with a cost of 5.
 * Do not mutate arguments
 * @param {ChessPiece[]} pieces existing autochess pieces
 * @returns {ChessPiece[]} duplicated array of pieces with Tidehunter
 *
 * @see ChessPiece
 */
export const arrayAdd = (pieces) => {
}

/** 
 * Add a Naga/Hunter piece named Tidehunter with a cost of 5.
 * Do mutate arguments. Does not return a result
 * @param {ChessPiece[]} pieces existing autochess pieces
 */
export const arrayAddMutative = (pieces) => {
}

/** 
 * Find the index of the piece with the specified name
 * @param {ChessPiece[]} pieces existing autochess pieces
 * @param {string} pieceName name of piece to find
 *
 * @returns {number} index of piece with pieceName
 */
export const arrayFindIndex = (pieces, pieceName) => {
}


/** 
 * Find the piece with the specified name
 * @param {ChessPiece[]} pieces existing autochess pieces
 * @param {string} pieceName name of piece to find
 *
 * @returns {ChessPiece} chess piece with pieceName
 */
export const arrayFind = (pieces, pieceName) => {
}
