import { cloneDeep } from 'lodash'

import { BASE_ARRAY, ADD_ARRAY_RESULT } from './arrayFixtures'
import { PIECES } from './baseFixtures'

import { arrayAdd, arrayAddMutative, arrayFind, arrayFindIndex } from '../src/array'

describe("Adding to Array", () => {
  it("should mutatively add Tidehunter to the given array", () => {
    const pieces = cloneDeep(BASE_ARRAY)
    arrayAddMutative(pieces)

    expect(pieces).toEqual(ADD_ARRAY_RESULT)
  })

  it("should immutably add Tidehunter to the given array", () => {
    const pieces = cloneDeep(BASE_ARRAY)
    const result = arrayAdd(pieces)

    expect(result).toEqual(ADD_ARRAY_RESULT)
    expect(pieces).toEqual(BASE_ARRAY)
  })

  it("should find index of Puck", () => {
    const pieces = cloneDeep(BASE_ARRAY)
    const index = arrayFindIndex(pieces, "Puck")

    expect(index).toEqual(1)
    expect(pieces).toEqual(BASE_ARRAY)
  })

  it("should return data of Puck", () => {
    const pieces = cloneDeep(BASE_ARRAY)
    const piece = arrayFind(pieces, "Puck")

    expect(piece).toEqual(PIECES.Puck)
    expect(pieces).toEqual(BASE_ARRAY)
  })
})
